"Only run if vim was compiled with autocmd enabled"
if has("autocmd")
  "If vim opened with no file to edit, start NERDTree"
  au VimEnter * if eval("@%") == "" | NERDTree | endif

  "turn on filetype detection"
  filetype on

  "perform specific configuration for a given filetype"
  autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

  "Start eclim plugin if the file is a java file"
  autocmd FileType java silent !~/.scripts/start_eclim.sh
  autocmd FileType java packadd eclim
endif

"Tab Settings (2 spaces as default)" 
set ts=2
set sts=2
set sw=2
set et

"Turn on line numbers"
set number
nmap <leader>n :set number!<CR>

"Turn off arrow keys"
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

"Map leader to spacebar"
let mapleader=" "

"Toggle white space display with \+l"
nmap <leader>l :set list!<CR>

"NERDTree config"
map <leader>f :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let NERDTreeShowHidden = 1

"Git Gutter config"
"Assumes black is the bg color for the terminal profile."
"Change the colorvalue of guibg if non-black"
"https://github.com/airblade/vim-gitgutter"
"https://github.com/airblade/vim-gitgutter/issues/614"
let g:gitgutter_map_keys = 0
let g:gitgutter_override_sign_column_highlight = 0
highlight clear SignColumn
highlight GitGutterAdd ctermfg=2
highlight GitGutterChange ctermfg=3
highlight GitGutterDelete ctermfg=1
highlight GitGutterChangeDelete ctermfg=4
highlight GitGutterAdd    guifg=#009900 guibg=#000000 
highlight GitGutterChange guifg=#bbbb00 guibg=#000000 
highlight GitGutterDelete guifg=#ff2222 guibg=#000000

set tags=tags
