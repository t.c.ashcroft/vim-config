
# Set Up
Clone this repo as follows:
`git clone --recurse-submodules https://gitlab.com/t.c.ashcroft/vim-config .vim`

ThensSymlink the vimrc file:
`ln -s ~/.vimrc/vimrc ~/.vimrc`
